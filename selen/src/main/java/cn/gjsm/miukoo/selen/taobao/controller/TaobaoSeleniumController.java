package cn.gjsm.miukoo.selen.taobao.controller;


import cn.gjsm.miukoo.selen.taobao.service.TaobaoSeleniumService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@RestController
@RequestMapping("/selen/taobao")
public class TaobaoSeleniumController {

    @Resource
    TaobaoSeleniumService taobaoSeleniumService;

    private static final Logger logger = LogManager.getLogger(TaobaoSeleniumController.class);

    @GetMapping("/getItemInfo")
    public Map<String,Object> getItemInfo(@RequestParam("url") String url){
        String decodedUrl = URLDecoder.decode(url, StandardCharsets.UTF_8);
        logger.info("input url: " + decodedUrl);
        return taobaoSeleniumService.getItemInfo(decodedUrl);
    }
}
