package cn.gjsm.miukoo.selen.taobao.service.impl;

import cn.gjsm.miukoo.selen.taobao.bean.TaobaoWebDriver;
import cn.gjsm.miukoo.selen.taobao.bean.TaobaoWebDriverManager;
import cn.gjsm.miukoo.selen.taobao.service.TaobaoSeleniumService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TaobaoSeleniumServiceImpl implements TaobaoSeleniumService {

    private static long SCROLL_WAIT = 500L;
    @Resource
    private TaobaoWebDriverManager taobaoWebDriverManager;

    private static final Logger logger = LogManager.getLogger(TaobaoSeleniumService.class);

    private static final String H5_LINK = "https://h5.m.taobao.com/awp/core/detail.htm?id=";

    private static String NEW_TAOBAO_SWIPER_WRAPPER = "swiper-wrapper";
    private static int SWIPE_WIDTH = -200;
    private static String NEW_TAOBAO_HTML = "#scrollViewContainer";
    private static String NEW_TAOBAO_HEIGHT_VIEW = ".rax-scrollview-webcontainer";
    @Override
    public Map<String, Object> getItemInfo(String url) {
        Map<String, Object> resMap = new HashMap<>();
//        // TODO: 暂时只处理http://m.tb.cn的移动链接
//        if (!url.matches(".*m\\.tb\\.cn.*")){
//            resMap.put("error",url);
//            return resMap;
//        }
//        // 处理pc链接
//        boolean h5 = false;
//        if (url.matches(".*item\\.taobao.*")||url.matches(".*detail\\.tmall.*")){
//            // 定义正则表达式
//            String regex = "[?&]id=(\\d+)";
//            Pattern pattern = Pattern.compile(regex);
//
//            // 进行匹配
//            Matcher matcher = pattern.matcher(url);
//
//            if (matcher.find()) {
//                String id = matcher.group(1);
//                url = H5_LINK + id;
//                h5 = true;
//            } else {
//                resMap.put("error","illegal pc link");
//                resMap.put("url",url);
//                return resMap;
//            }
//        }
        TaobaoWebDriver taobaoWebDriver = taobaoWebDriverManager.getAvailableDriver();
        
        try {
            WebDriver driver = taobaoWebDriver.getWebDriver();

            // 访问用户商品信息
            driver.get(url);
            WebDriverWait wait = new WebDriverWait(driver, 5); // 设置最大等待时间为10秒

            // 使用等待机制等待页面所有元素可见
            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img")));
            }catch (Exception e){
                logger.info("time limit exceeds, current url: " + driver.getCurrentUrl());
            }
//        driver.get("https://m.tb.cn/h.50Shuyk?tk=HMwTdHELwH0"); // 天猫
//        driver.get("https://m.tb.cn/h.50Si8B3?tk=2ImHdHEJ5I7"); // 淘宝
//        driver.get("https://m.tb.cn/h.5cSz4S9?tk=bucIduCoqwb"); // new taobao
            List<WebElement> elements;
            logger.info("begin scanning, current url " + driver.getCurrentUrl());

            // 使用JavaScript模拟页面滚动，直到没有新内容加载
            JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
            WebElement html;
            WebElement heightView;
//            if (h5) {
//                html = driver.findElement(By.id("ice-container"));
//            }else{
//                html = driver.findElement(By.tagName("html"));
//            }
            String currentUrl = driver.getCurrentUrl();
            if (currentUrl.matches(".*new\\.m\\.taobao.*")) {
                // 淘宝手机页面在电脑访问
                // 获取swiper-wrapper元素
                WebElement swiperWrapper = driver.findElement(By.className(NEW_TAOBAO_SWIPER_WRAPPER));


                // 创建Actions实例并拖动swiper-wrapper
                Actions actions = new Actions(driver);
                actions.clickAndHold(swiperWrapper).moveByOffset(SWIPE_WIDTH, 0).release().perform();

                html = driver.findElement(By.cssSelector(NEW_TAOBAO_HTML));
                heightView = driver.findElement(By.cssSelector(NEW_TAOBAO_HEIGHT_VIEW));
            }else{
                // 其余种类页面
                html = driver.findElement(By.tagName("html"));
                heightView = html;
            }

            // 滑动页面并不断加载新的高度直到到达页面底
            int height = heightView.getSize().getHeight();
            logger.info("current height" + height);
            for (int h = 0; h < height ; h += 1000){
                jsExecutor.executeScript("arguments[0].scrollBy(0,1000);",html);
                height = heightView.getSize().getHeight();
                logger.info("current height" + height);
                Thread.sleep(SCROLL_WAIT);
            }
            if (currentUrl.matches(".*new\\.m\\.taobao.*")){
                // 淘宝手机页面在电脑端
                WebElement temp = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".swiper-wrapper")));
                WebElement desc = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".detail-desc")));
                elements = temp.findElements(By.cssSelector("img"));
                elements.addAll(desc.findElements(By.cssSelector("img")));
//            if (h5){
//                WebElement desc = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#desc")));
//                elements = desc.findElements(By.cssSelector("img"));
            } else if (currentUrl.matches(".*item\\.taobao.*")) {
                // 淘宝原生
                WebElement temp = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".tb-gallery")));
                WebElement desc = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#description")));
                elements = temp.findElements(By.cssSelector("img"));
                elements.addAll(desc.findElements(By.cssSelector("img")));
            } else if (currentUrl.matches(".*detail\\.tmall.*")) {
                // 天猫
                WebElement temp = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[class^=\"PicGallery\"]")));
                WebElement desc = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".desc-root")));
                elements = temp.findElements(By.cssSelector("img"));
                elements.addAll(desc.findElements(By.cssSelector("img")));
            } else {
                elements = driver.findElements(By.cssSelector("img"));
            }
            Pattern pattern = Pattern.compile("(.+?\\.(?:jpg|png|jpeg)).*");
            Set<String> res = new HashSet<>();
            for (WebElement e : elements) {
                Matcher matcher = pattern.matcher(e.getAttribute("src"));
                if (matcher.find()) {

                    res.add(matcher.group(1));
                }
            }

            //返回数据存放
            resMap.put("img", res);
            resMap.put("url", currentUrl);
            resMap.put("originalTitle", driver.getTitle());

            Pattern t = Pattern.compile("^(.*)-[^-]*$");
            Matcher titleM = t.matcher(driver.getTitle());
            if (titleM.find()) {
                resMap.put("title", titleM.group(1));
            }else{
                resMap.put("title", driver.getTitle());
            }
//            if (h5) {
//                resMap.put("title",driver.findElement(By.cssSelector("h2.title")).getText());
//            }

            return resMap;
        }catch (Exception e) {
            logger.error("current error page: " + taobaoWebDriver.getWebDriver().getCurrentUrl());
            logger.error(e.getMessage());
            logger.error(taobaoWebDriver.getWebDriver().getPageSource());
//            taobaoWebDriver.setWorking(false);
        }finally {
            taobaoWebDriver.releaseWebDriver();
        }
        resMap.put("error",taobaoWebDriver.getWebDriver().getCurrentUrl());
        return resMap;
    }

    @Override
    public void closeChrome() {
        taobaoWebDriverManager.closeAll();
    }
}
