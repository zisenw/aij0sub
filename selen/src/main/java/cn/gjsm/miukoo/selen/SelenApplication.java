package cn.gjsm.miukoo.selen;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

@SpringBootApplication
public class SelenApplication {

    private static final Logger logger = LogManager.getLogger(SelenApplication.class);

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SelenApplication.class);
        application.addListeners(new ApplicationShutdownListener());
        application.run(args);
    }

    // 释放资源
    private static class ApplicationShutdownListener implements ApplicationListener<ContextClosedEvent> {
        @Override
        public void onApplicationEvent(ContextClosedEvent event) {
            logger.info("springboot application will close");
            // 启动Spring Boot应用的结束应用生命周期
            SpringApplication.exit(event.getApplicationContext());
        }
    }
}
