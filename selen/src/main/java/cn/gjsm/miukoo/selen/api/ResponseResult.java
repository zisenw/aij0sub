package cn.gjsm.miukoo.selen.api;

import java.io.Serializable;

public class ResponseResult implements Serializable {
    /**
     * 错误code，默认是0(成功)，调用方依据此code判断后端api是否处理成功
     */
    private int code = 0;

    private boolean success = true;

    /**
     * 错误信息
     */
    private String message = "ok";

    private boolean fail = false;

    /**
     * 数据对象
     */
    private Object data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isFail() {
        return fail;
    }

    public void setFail(boolean fail) {
        this.fail = fail;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResponseResult(int code, boolean success, String message, boolean fail, Object data) {
        this.code = code;
        this.success = success;
        this.message = message;
        this.fail = fail;
        this.data = data;
    }

    public ResponseResult(int code) {
        this.code = code;
    }

    public ResponseResult(Object data) {
        this.data = data;
    }

    public ResponseResult(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResponseResult() {

    }
}
