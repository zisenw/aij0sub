package cn.gjsm.miukoo.selen.taobao.service;

import java.util.Map;

public interface TaobaoSeleniumService {
    Map<String,Object> getItemInfo(String url);
    void closeChrome();
}
