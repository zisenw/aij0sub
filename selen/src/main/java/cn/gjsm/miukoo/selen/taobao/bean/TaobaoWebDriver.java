package cn.gjsm.miukoo.selen.taobao.bean;

import cn.gjsm.miukoo.selen.api.SeleniumException;
import cn.gjsm.miukoo.selen.taobao.service.TaobaoSeleniumService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Scope("prototype")
public class TaobaoWebDriver implements InitializingBean {
    private static final String LOCAL_IMG = "/selenium/qrcode/";
    //    private static final String LOCAL_IMG ="C:\\Users\\96203\\Desktop\\";
    private static long QR_AUTH_WAIT = 1000 * 60 * 2;
    private final String ID = get32UUID();

    @Value("${selenium.log-location}")
    private String SELENIUM_LOG_LOCATION;

    // 检修间隔
    public static long CHECK_INTERVAL_MIN = 2*60*1000L;
    public static int CHECK_INTERVAL_OS = 3*60*1000;
    private static final Logger logger = LogManager.getLogger(TaobaoSeleniumService.class);

    private WebDriver driver;

    private AtomicBoolean occupied = new AtomicBoolean(false);
    private boolean working = true;

    /*
        WebDriver初始化方法
     */
    @PostConstruct
    public void init(){
        System.setProperty("webdriver.chrome.driver", "/selenium/chromedriver");
//        System.setProperty("webdriver.chrome.driver", "C:\\Windows\\chromedriver.exe");

        File logFile = new File(SELENIUM_LOG_LOCATION);

        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // 处理文件创建失败的异常
                logger.info(e);
            }
        }
        ChromeDriverService service = new ChromeDriverService.Builder()
                .withLogFile(logFile)
                .build();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless=new");
        options.addArguments("--disable-gpu");
        options.addArguments("--window-size=1500,1000");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-blink-features=AutomationControlled");
        options.addArguments("user-agent='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'");
        driver = new ChromeDriver(service,options);
        logger.info("driver " + ID + " : start auth");
//        getAuth();
        getQRCodeAuth();
        logger.info("driver " + ID + " : setup done");
    }

    private void getQRCodeAuth(){
        ByteArrayInputStream byteArrayInputStream = null;
        try {

            driver.get("https://login.taobao.com");
            Thread.sleep(1000);

            // 获取qrcode
            WebElement qr = driver.findElement(By.cssSelector(".icon-qrcode"));
            qr.click();
            Thread.sleep(1000);

            // 获取图片内容
            // 找到包含Canvas的div元素
            WebElement divElement = driver.findElement(By.cssSelector(".qrcode-img"));

            // 找到Canvas元素
            WebElement canvas = divElement.findElement(By.tagName("canvas"));

            // 使用JavaScript将Canvas内容绘制到画布并获取Base64编码的图片数据
            String canvasBase64 = (String) ((JavascriptExecutor) driver).executeScript(
                    "var dataURL = arguments[0].toDataURL('image/png').replace(/^data:image\\/png;base64,/, '');" +
                            "return dataURL;", canvas);

            // 解码Base64编码的图片数据并保存为图片文件
            byte[] imageBytes = Base64.getDecoder().decode(canvasBase64);
            byteArrayInputStream = new ByteArrayInputStream(imageBytes);
            BufferedImage image = ImageIO.read(byteArrayInputStream);
            File output = new File(LOCAL_IMG + ID + ".png"); // 保存图片，替换为你希望保存图片的路径和文件名
            ImageIO.write(image, "png", output);
            logger.info("please scan qrcode");
            Thread.sleep(QR_AUTH_WAIT);
        } catch (Exception e) {
            logger.error(e);
        }finally {
            if (byteArrayInputStream != null){
                try {
                    byteArrayInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getAuth(){
        driver.get("https://taobao.com");
        try {
            Thread.sleep(2*1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.get("https://login.taobao.com");

        // 检查是否已经登陆
        WebElement loginButton = driver.findElement(By.cssSelector("button.fm-button"));
        if ("快速进入".equals(loginButton.getText())){
            loginButton.click();
        }else{
            // 输入用户名
            WebElement idInput = driver.findElement(By.id("fm-login-id"));
            idInput.click();
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            idInput.sendKeys("tb143524484");

            try {
                Thread.sleep(2 * 1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 输入密码
            WebElement passwordInput = driver.findElement(By.id("fm-login-password"));
            passwordInput.click();
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            passwordInput.sendKeys("943761TTy");
            try {
                Thread.sleep(2 * 1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            WebDriverWait wait = new WebDriverWait(driver, 10);
//            try {
//                WebElement div = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("nc_1__bg")));
//                if (div.isDisplayed()){
//                    logger.info("slide bar appears");
//                    Thread.sleep(2000); // Java中使用Thread.sleep()来等待
//
//                    Actions actions = new Actions(driver);
//                    actions.clickAndHold(div).perform();
//                    actions.moveByOffset(300, 0).perform();
//
//                    wait.until(ExpectedConditions.textToBePresentInElementLocated(
//                            By.cssSelector("div#nc_1__scale_text > span.nc-lang-cnt > b"), "验证通过"));
//                    Thread.sleep(1000);
//                }
//            } catch (Exception e) {
//                logger.error("error occurs when passing verification bar");
//                logger.error(e);
//            }

            // 登陆
            loginButton.click();
        }
        try {
            Thread.sleep(5*1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String currentUrl = driver.getCurrentUrl();
        logger.info("driver " + ID + " handle login:" + currentUrl);
    }

    public TaobaoWebDriver getTaobaoDriver() throws SeleniumException {
        if (working && tryOccupied()){
            logger.info(ID + " driver is achieved.");
            return this;
        }else{
            throw new SeleniumException("WebDriver is occupied. ID: "+ID);
        }
    }
    public void releaseWebDriver(){
        logger.info(ID + " driver is released.");
        occupied.set(false);
    }

    public void close(){
        driver.close();
        logger.info("driver " + ID + " is closed.");
    }

    public String getID() {
        return ID;
    }

    public static String get32UUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    @PreDestroy
    public void cleanup(){
        close();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        new Thread(()->{
            while (true) {
                if (tryOccupied()) {
                    try {
                            logger.info("driver " + ID + " in background thread, current page: " + driver.getCurrentUrl());
//                                // 检修时暂停服务
//                                String currentUrl = driver.getCurrentUrl();
//                                boolean inWorld = currentUrl.matches(".*world\\.taobao\\.com.*");
//                                boolean inTaobao = currentUrl.matches(".*item\\.taobao.*");
//                                boolean inTMall = currentUrl.matches(".*detail\\.tmall.*");
//                                boolean inLogin = currentUrl.matches(".*my_taobao.*");
//                                boolean inUnusual = currentUrl.matches(".*login_unusual\\.htm.*");
//                                if (!(inLogin || inTaobao || inTMall || inUnusual || inWorld)) {
//                                    logger.info("driver " + ID + " webpage exception, current page: " + driver.getCurrentUrl());
//                                    working = false;
//                                    getAuth();
//                                } else {
//                                    working = true;
//                                }

                    } catch (Exception e) {
                        logger.info("fixing thread " + getID() + " encounters an issue");
                        logger.info(e);
                    } finally {
                        // 检修后恢复服务
                        releaseWebDriver();
                    }
                }else{
                    logger.info("try occupied fail");
                }
                try {
                    Thread.sleep(CHECK_INTERVAL_MIN + new Random().nextInt(CHECK_INTERVAL_OS));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }

    private boolean tryOccupied() {
        boolean res = occupied.compareAndSet(false,true);
        logger.info(Thread.currentThread().getName() + " thread tried to occupied, res: " + res);
        return res;
    }

    public WebDriver getWebDriver(){
        return driver;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }
}
