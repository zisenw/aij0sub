package cn.gjsm.miukoo.selen.api;

import java.io.Serializable;

public class SeleniumException extends Exception implements Serializable {

    public SeleniumException(){
        super();
    }

    public SeleniumException(String msg){
        super(msg);
    }
}
