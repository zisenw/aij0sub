package cn.gjsm.miukoo.selen.taobao.bean;

import cn.gjsm.miukoo.selen.api.SeleniumException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class TaobaoWebDriverManager {
    // driver数量
//    private static final int NUM_DRIVERS = 2;
    private static final int NUM_DRIVERS = 1;

    // 轮询间隔
    public static long INTERVAL = 500L;

    private static final Logger logger = LogManager.getLogger(TaobaoWebDriverManager.class);
    private static final Map<String, TaobaoWebDriver> drivers = new ConcurrentHashMap<>();

    @Resource
    private ApplicationContext applicationContext;

    @PostConstruct
    public void init(){
        logger.info("drivers init...");
        for (int i=0;i<NUM_DRIVERS;i++){
            TaobaoWebDriver driver = applicationContext.getBean(TaobaoWebDriver.class);
            drivers.put(driver.getID(),driver);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public TaobaoWebDriver getAvailableDriver(){
        while(true){
            // 遍历所有driver
            for (TaobaoWebDriver taobaoWebDriver : drivers.values()){
                try {
                    return taobaoWebDriver.getTaobaoDriver();
                } catch (SeleniumException ignored) {
                }
            }
            // 没有可用driver，则间隔一定时间后轮询
            try {
                Thread.sleep(INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @PreDestroy
    public void closeAll(){
        logger.info("close all webdriver");
        for (TaobaoWebDriver taobaoWebDriver : drivers.values()){
            taobaoWebDriver.close();
        }
    }
}
